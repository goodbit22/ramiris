# Ramiris

Simple scanner using nmap

![graphic1](./img/1.png)

## Table of Contents

- [Ramiris](#ramiris)
  - [Table of Contents](#table-of-contents)
  - [Installation](#installation)
  - [Running](#running)
  - [Technologies](#technologies)
  - [Authors](#authors)
  - [License](#license)

## Installation

Before running the script for the first time, run the commands

```python3
 pip3 install -r requirements.txt
```

```sh
  sudo apt install python3-termcolor nmap
```

## Running

```python3
 python3 ramiris.py 
```

## Technologies

Project is created with:

- python 3.10
- pyfiglet
- colored
- termcolor

## Authors

***
__goodbit22__ --> 'https://gitlab.com/users/goodbit22'
***

## License

  Apache License Version 2.0
