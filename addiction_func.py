from os import mkdir, path
from subprocess import PIPE, Popen
from ipaddress import ip_address

def create_directory(directory_name="scan_result") -> None:
    """
    Function create directory
    """
    try:
        if not path.isdir(directory_name):
            mkdir(directory_name)
    except OSError as error:
        print(error)


def insert_ipaddress() -> (str | None):
    """
    Function check ip address
    """
    def check_ip(ip_addr):
        process = Popen(['ping', '-c', '2', ip_addr ], stderr=PIPE, stdout=PIPE)
        response=process.wait()
        return response
    status=None
    while status != 0:
        while True:
            try:
                host = input("Please enter the ip address: ")
                ip_address(host)
                break
            except ValueError:
                print("Not a valid IP address")
        status = check_ip(host)
        if status == 0:
            print("Status: up ")
            return host
        print("Status: down")

def available_host(host  : str, osfamily : str, filename='hosts_up.txt') -> None:
    """
    Function check  the address in  the host_up.txt file
    """
    if not path.exists(filename):
        with open(filename, 'w', encoding="utf-8"): pass
    def new_host(new_host):
        with open(filename, 'a+', encoding="utf-8") as append_file_hosts:
            append_file_hosts.write(new_host + '\n')

    def read_file():
        hosts = []
        with open(filename, 'r', encoding="utf-8") as read_file_hosts:
            hosts= [line.replace('\n','') for line in read_file_hosts]
        return hosts
    hosts = read_file()
    if host + ':' + osfamily not in hosts:
        new_host(host+':'+osfamily)