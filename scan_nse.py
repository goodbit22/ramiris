# import nmap3
from os import listdir
from termcolor import colored
import nmap3


class ScanNse:
    """
    class
    """

    def __init__(self, ip_address: str, filename: str) -> None:
        """
        """
        self.ip_address = ip_address
        self.filename = filename
        self.nse_lists = self.__nse_list()

    def __nse_list(self) -> list[str]:
        """
        """
        path_nse = '/usr/share/nmap/scripts'
        return listdir(path_nse)

    def __overwrite_file(self, result: str) -> None:
        """
        """
        with open(f"scan_result/{self.filename}", 'a', encoding="utf-8") as file_result:
            file_result.write(result + '\n')

    def nse_rdp(self) -> None:
        """
        Function that scanning rdp service with nse script
        """
        nmap = nmap3.Nmap()
        nse_scripts_rdp = [nse_rdp for nse_rdp in self.nse_lists if nse_rdp.startswith("rdp")]
        self.__overwrite_file('-' * 10 + "scanning rdp service with nse script" + '-' * 10)
        print(colored('-' * 10 + "scanning rdp service with nse scripts" + '-' * 10, "cyan"))
        for nse_script in nse_scripts_rdp:
            print(colored(f"*** NSE script: {nse_script} ***", "grey"))
            self.__overwrite_file(f"*** NSE script: {nse_script} ***\n")
            result_nse_scan = nmap.nmap_version_detection(self.ip_address, args=f"-p 3389 --script {nse_script} ")
            self.__overwrite_file(result_nse_scan)

    def nse_ftp(self) -> None:
        """
        Function that scanning ftp service with nse script
        """
        nmap = nmap3.Nmap()
        print(colored('-' * 10 + "Scanning ftp service with nse script" + '-' * 10, "cyan"))
        nse_scripts_ftp = [nse_ftp for nse_ftp in self.nse_lists if nse_ftp.startswith("ftp")]
        self.__overwrite_file('-' * 10 + "Scanning ftp service with nse script" + '-' * 10)
        for nse_script in nse_scripts_ftp:
            print(colored(f"*** NSE script: {nse_script} ***", "grey"))
            result_nse_scan = nmap.nmap_version_detection(self.ip_address, args=f"-p 21 --script {nse_script}")
            self.__overwrite_file(f"*** NSE script: {nse_script} ***\n")
            self.__overwrite_file(result_nse_scan)

    def nse_smb(self, osfamily) -> None:
        """
        Function that scanning smb service with nse script
        """
        directory = '/usr/share/wordlists'
        nmap = nmap3.Nmap()
        nse_scripts_smb = [nse_smb for nse_smb in self.nse_lists if nse_smb.startswith("smb")]
        print(colored('-' * 10 + "Scanning smb service with nse script" + '-' * 10, "cyan"))
        self.__overwrite_file('-' * 10 + "Scanning smb service with nse script" + '-' * 10)
        for nse_script in nse_scripts_smb:
            print(colored(f"*** NSE script: {nse_script} ***", "grey"))
            self.__overwrite_file(f"*** NSE script: {nse_script} ***\n")
            if nse_script == 'smb-vuln-cve-2017-7494':
                result_nse_scan = nmap.nmap_version_detection(self.ip_address, args=f"-p 445 --script {nse_script}")
                print(result_nse_scan)
                self.__overwrite_file(result_nse_scan)
                result_nse_scan = nmap.nmap_version_detection(self.ip_address,
                                                              args=f"-p 445 --script {nse_script} --script-args=smb"
                                                                   f"-vuln-cve-2017-7494.check-version")
                print(result_nse_scan)
                self.__overwrite_file(result_nse_scan)
            elif nse_script == 'smb-enum-users.nse' or nse_script == 'smb-enum-processes' \
                    or nse_script == 'smb-security-mode':
                result_nse_scan = nmap.nmap_version_detection(self.ip_address, args=f"-p 445 --script {nse_script}")
                print(result_nse_scan)
                self.__overwrite_file(result_nse_scan)
                result_nse_scan = nmap.nmap_version_detection(self.ip_address,
                                                              args=f"-sU -sS -p U:137,T:139 --script {nse_script}")
                print(result_nse_scan)
                self.__overwrite_file(result_nse_scan)
            elif nse_script == 'smb-enum-services' or nse_script == 'smb-vuln-ms17-010':
                if osfamily == 'Windows':
                    result_nse_scan = nmap.nmap_version_detection(self.ip_address,
                                                                  args=f"-p 445  --script {nse_script}")
                    print(result_nse_scan)
                    self.__overwrite_file(result_nse_scan)
                    result_nse_scan = nmap.nmap_version_detection(self.ip_address,
                                                                  args=f"-p U:137,T:139  --script {nse_script}")
                    print(result_nse_scan)
                    self.__overwrite_file(result_nse_scan)
            elif nse_script == "smb-brute":
                result_nse_scan = nmap.nmap_version_detection(self.ip_address,
                                                              args=f"-p 445  --script {nse_script} --script-args=userdb={directory}/metasploit/unix_users.txt passdb={directory}/nmap.lst")
                print(result_nse_scan)
                self.__overwrite_file(result_nse_scan)
            elif nse_script == 'smb-ls':
                result_nse_scan = nmap.nmap_version_detection(self.ip_address, args=f"-p 445  --script {nse_script}")
                print(result_nse_scan)
                self.__overwrite_file(result_nse_scan)
            elif nse_script == 'smb-vuln-ms06-025' or nse_script == 'smb-vuln-ms07-029' \
                    or nse_script == 'smb-vuln-ms08-067' or nse_script == 'smb-vuln-cve2009-3103' \
                    or nse_script == 'smb-vuln-ms10-054' or nse_script == 'smb-vuln-ms10-061':
                result_nse_scan = nmap.nmap_version_detection(self.ip_address,
                                                              args=f"-p U:137,U:139,T:139,T:445  --script {nse_script}")
                print(result_nse_scan)
                self.__overwrite_file(result_nse_scan)
            else:
                result_nse_scan = nmap.nmap_version_detection(self.ip_address,
                                                              args=f"-p 445,139  --script {nse_script}")
                print(result_nse_scan)
                self.__overwrite_file(result_nse_scan)

    def nse_snmp(self) -> None:
        """
        Function that scanning snmp service with nse script
        """
        nmap = nmap3.Nmap()
        nse_scripts_snmp = [nse_snmp for nse_snmp in self.nse_lists if nse_snmp.startswith("snmp")]
        print(colored('-' * 10 + "Scanning snmp service with nse script" + '-' * 10, "cyan"))
        self.__overwrite_file('-' * 10 + "Scanning snmp service with nse script" + '-' * 10)
        for nse_script in nse_scripts_snmp:
            print(colored(f"*** NSE script: {nse_script} %s ***", "grey"))
            results = nmap.nmap_version_detection(self.ip_address, args=f"-p 161 -sU --script {nse_script}")
            print(results)
            self.__overwrite_file(f"*** NSE script: {nse_script} ***\n")
            self.__overwrite_file(results)

    def nse_third_parties(self):
        """
        Function that scanning services with nse script third parties
        """
        nmap = nmap3.Nmap()
        with open(f"scan_result/{self.filename}", 'a', encoding="utf-8") as file_result:
            print(colored('-' * 10 + "Scanning services with nse script third parties" + '-' * 10, "cyan"))
            file_result.write("\n" + '-' * 10 + "Scanning services with nse script third parties" + '-' * 10 + '\n')
            nse_scripts_third_parties = ('vulscan/vulscan.nse', 'vulners')
            for nse_script in nse_scripts_third_parties:
                print(colored(f"*** NSE script: {nse_script} ***", "grey"))
                results = nmap.nmap_version_detection(self.ip_address, args=f"--script {nse_script}")
                file_result.write(f"*** NSE script: {nse_script} ***\n")
                file_result.write(results + '\n')

    def nse_ssh(self) -> None:
        """
        Function that scanning ssh service with nse script
        """
        directory = '/usr/share/wordlists'
        nmap = nmap3.Nmap()
        nse_scripts_ssh = [nse_ssh for nse_ssh in self.nse_lists if nse_ssh.startswith("ssh")]
        print(colored('-' * 10 + "Scanning ssh service with nse script" + '-' * 10, "cyan"))
        self.__overwrite_file('-' * 10 + "Scanning ssh service with nse script" + '-' * 10)
        for nse_script in nse_scripts_ssh:
            print(colored(f"*** NSE script: {nse_script} ***", "grey"))
            self.__overwrite_file(f"*** NSE script: {nse_script} ***\n")
            if nse_script == "ssh-brute":
                result_nse_scan = nmap.nmap_version_detection(self.ip_address, args=f"--script {nse_script} -p 22 \
                --script-args userdb={directory}/metasploit/unix_users.txt \
                passdb={directory}s/metasploit/unix_passwords.txt --script-args ssh-brute.timeout=4s")
                print(result_nse_scan)
                self.__overwrite_file(result_nse_scan)
            elif nse_script == "ssh-auth-methods":
                result_nse_scan = nmap.nmap_version_detection(self.ip_address,
                                                              args=f" -p 22 --script {nse_script} --script-args=ssh"
                                                                   f".user={directory}/metasploit/unix_users.txt")
                print(result_nse_scan)
                self.__overwrite_file(result_nse_scan)
            else:
                result_nse_scan = nmap.nmap_version_detection(self.ip_address,
                                                              args=f" -p 22 --script {nse_script}")
                print(result_nse_scan)
                self.__overwrite_file(result_nse_scan)

    def nse_smtp(self) -> None:
        """
        Function that scanning smtp service with nse script
        """
        nmap = nmap3.Nmap()
        nse_scripts_smtp = [nse_smtp for nse_smtp in self.nse_lists if nse_smtp.startswith("smtp")]
        print(colored('-' * 10 + "Scanning smtp service with nse script" + '-' * 10, "cyan"))
        self.__overwrite_file('-' * 10 + "Scanning smtp service with nse script" + '-' * 10)
        for nse_script in nse_scripts_smtp:
            print(colored(f"*** NSE script: {nse_script} ***", "grey"))
            result_nse_scan = nmap.nmap_version_detection(self.ip_address,
                                                          args=f"-p 25,465,587 --script {nse_script}")
            print(result_nse_scan)
            self.__overwrite_file(f"*** NSE script: {nse_script} ***\n")
            self.__overwrite_file(result_nse_scan)

    def nse_telnet(self) -> None:
        """
        Function that scanning telnet service with nse script
        """
        nmap = nmap3.Nmap()
        nse_scripts_telnet = [nse_telnet for nse_telnet in self.nse_lists if nse_telnet.startswith("telnet")]
        print(colored('-' * 10 + "Scanning telnet service with nse script" + '-' * 10, "cyan"))
        self.__overwrite_file('-' * 10 + "Scanning telnet service with nse script" + '-' * 10)
        for nse_script in nse_scripts_telnet:
            print(colored(f"*** NSE script: {nse_script} ***", "grey"))
            directory = '/usr/share/wordlists'
            result_nse_scan = nmap.nmap_version_detection(self.ip_address,
                                                          args=f"-p 23 --script {nse_script}"
                                                               f" --script-args userdb={directory}/metasploit/unix_users.txt, passdb={directory}/nmap.lst")
            print(result_nse_scan)
            self.__overwrite_file(f"*** NSE script: {nse_script} ***\n")
            self.__overwrite_file(result_nse_scan)
