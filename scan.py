from termcolor import colored
from nmap3 import Nmap
from scan_nse import ScanNse
from addiction_func import available_host


class Scan(ScanNse):
    """
      class
    """

    def __init__(self, ip_address, filename: str) -> None:
        self.ip_address = ip_address
        self.filename = filename
        super().__init__(ip_address, filename)

    def scan(self, results, scan_type: str):
        """
        """
        osfamily = ""
        print(
            colored(':' * 20 + f"Start {scan_type} type scanning to  {self.ip_address} address" + ':' * 20, "green"))
        self.__overwrite_file(':' * 20 + f"Start {scan_type} type scanning to  {self.ip_address} address" + ':' * 20)
        # print(results)
        if results[self.ip_address]['hostname']:
            hostname = results[self.ip_address]['hostname'][0]['name']
            self.__overwrite_file(f"Hostname: {hostname}")
            print(f"Hostname: {hostname}")
        if results[self.ip_address]['osmatch']:
            osfamily = results[self.ip_address]['osmatch'][0]['osclass']['osfamily']
            print(osfamily)
            self.__overwrite_file(f'OSfamily: {osfamily}')
            print(f'OSfamily: {osfamily}')
            available_host(self.ip_address, osfamily)
        print("PORT \t State \t Service \t Product \t Version")
        ports = []
        for port in results[self.ip_address]['ports']:
            if port:
                print('%s\t %s\t %s\t %s\t %s' % (
                    port['portid'], port['state'], port['service']['name'], port['service']['product'],
                    port['service']['version']
                ))
                ports.append(port['portid'])
                self.__overwrite_file("PORT \t State \t Service \t Product \t Version")
                self.__overwrite_file('%s\t %s\t %s\t %s\t %s\n' % (
                    port['portid'], port['state'], port['service']['name'], port['service']['product'],
                    port['service']['version']
                ))
        self.__has_ports(ports, osfamily)

    def is_firewall(self) -> None:
        """
        """
        nmap = Nmap()
        print(colored('-' + '>' * 12 + "Rozpoczento skanowanie w celu wykrycia firewall" + '<' * 12, "cyan"))
        result = nmap.nmap_detect_firewall(target=self.ip_address)
        print(f"Firewall: {result}")
        with open(f"scan_result/{self.filename}", 'a') as file_result:
            file_result.write('-' + '>' * 12 + "Rozpoczento skanowanie w celu wykrycia firewall" + '<' * 12 + '-\n')
            file_result.write(f"Firewall: {result}")

    def __has_ports(self, ports, osfamily):
        if '21' in ports or '20' in ports:
            print('Locate ftp service')
            super().nse_ftp()
        if '23' in ports:
            print('Locate telnet service')
            super().nse_telnet()
        if '22' in ports:
            print('Locate ssh service')
            super().nse_ssh()
        if '25' in ports or '587' in ports or '2525' in ports:
            print('Locate smtp service')
            super().nse_smtp()
        if '445' in ports or '139' in ports:
            print('Locate smb service')
            super().nse_smb(osfamily)
        if '3389' in ports:
            print('Locate rdp service')
            super().nse_rdp()

    def __overwrite_file(self, result: str):
        with open(f"scan_result/{self.filename}", 'a', encoding="utf-8") as file_result:
            file_result.write(result + '\n')
