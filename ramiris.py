#!/usr/bin/python3  env

from datetime import datetime
from os import getuid, path
from pyfiglet import figlet_format
from termcolor import colored
import nmap3
from addiction_func import insert_ipaddress, create_directory
from scan import Scan
from scan_nse import ScanNse


def menu() -> None:
    """
    Function display main menu
    """
    print("\n")
    print(colored("------------------------ Menu Ramiris ------------------------", "magenta"))
    print("""
    1)SYN Scan
    2)UDP Scan
    3)FIN Scan
    4)INTENSE Scan
    5)All types scans
    6)EXIT
    """)


def syn(ip_address):
    """
    nmap scan syn
    """
    nmap = nmap3.NmapScanTechniques()
    return nmap.nmap_syn_scan(ip_address, args="-sV -O")


def udp(ip_address):
    """
    nmap scan udp
    """
    nmap = nmap3.NmapScanTechniques()
    return nmap.nmap_udp_scan(ip_address, args="-sV -O")


def fin(ip_address):
    """
    nmap scan udp
    """
    nmap = nmap3.NmapScanTechniques()
    return nmap.nmap_fin_scan(ip_address, args="-sV -O")


def idle(ip_address):
    """
    nmap scan idle
    """
    nmap = nmap3.NmapScanTechniques()
    return nmap.nmap_idle_scan(ip_address, args="-sV -O")


def name(ip_address, scan_type: str) -> str:
    """
    Function that generate file name with the scan result
    """
    now = datetime.now()
    return ip_address + '_scan_result_' + scan_type + now.strftime("%H:%M:%S") + '.txt'


def main() -> None:
    """
    Main function
    """
    scans_type = ('Syn', 'UDP', 'FIN', 'IntenseScan', 'All')
    option = 22
    while option != '6':
        menu()
        option = str(input("Select option: "))
        if option == '1':
            ip_address = insert_ipaddress()
            filename = name(ip_address, scans_type[0])
            result = syn(ip_address)
            scan = Scan(ip_address, filename)
            scan.scan(result, scans_type[0])
            scan_nse = ScanNse(ip_address, filename)
            scan_nse.nse_third_parties()
            scan.is_firewall()
        elif option == '2':
            ip_address = insert_ipaddress()
            udp(ip_address)
            filename = name(ip_address, scans_type[1])
            result = syn(ip_address)
            scan = Scan(ip_address, filename)
            scan.scan(result, scans_type[1])
            scan_nse = ScanNse(ip_address, filename)
            scan_nse.nse_third_parties()
            scan.is_firewall()
        elif option == '3':
            ip_address = insert_ipaddress()
            filename = name(ip_address, scans_type[2])
            result = fin(ip_address)
            scan = Scan(ip_address, filename)
            scan.scan(result, scans_type[2])
            scan.is_firewall()
            scan_nse = ScanNse(ip_address, filename)
            scan_nse.nse_third_parties()
        elif option == '4':
            ip_address = insert_ipaddress()
            filename = name(ip_address, scans_type[3])
            result = idle(ip_address)
            scan = Scan(ip_address, filename)
            scan.scan(result, scans_type[3])
            scan.is_firewall()
            scan_nse = ScanNse(ip_address, filename)
            scan_nse.nse_third_parties()
        elif option == '5':
            ip_address = insert_ipaddress()
            filename = name(ip_address, scans_type[4])
            scan = Scan(ip_address, filename)
            result = syn(ip_address)
            scan.scan(result, scans_type[4])
            result = udp(ip_address)
            scan.scan(result, scans_type[4])
            result = fin(ip_address)
            scan.scan(result, scans_type[4])
            result = idle(ip_address)
            scan.scan(result, scans_type[4])
            scan_nse = ScanNse(ip_address, filename)
            scan_nse.nse_third_parties()
        elif option == '6':
            print("EXIT")
        else:
            print(chr(27) + "[2J")


if __name__ == '__main__':
    if getuid() == 0:
        ascii_banner = figlet_format("Ramiris")
        print(colored(ascii_banner, 'blue'))
        print(colored("Created by goodbit22", "green"))
        create_directory()
        try:
            main()
        except KeyboardInterrupt:
            print("\nEXIT")
    else:
        print("you don't have root rights, run the script with root rights")
